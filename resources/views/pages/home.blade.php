@extends('layout')





@section('content')

<div class="jumbotron">
		<h1 class="display-3">Eloquent Basico</h1>
		<p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
		<hr class="my-4">
		<p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
		<p class="lead">
			<a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
		</p>
	</div>
<div class="container">
	
<h2>Ultimos usuarios registrados</h2>
<table class="table table-hover table-striped">
	<thead>
		<tr>
			<th>ID</th>
		<th>Nombre</th>
		<th>Email</th>
		<th>Genero</th>
		<th>Biografia</th>
			
		</tr>
		
	</thead>
	<tbody>

		@include('query.partials.list-users')
		
	</tbody>

</table>
	
</div>
	

@stop
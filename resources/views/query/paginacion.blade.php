@extends('layout')





@section('content')

<div class="container">
	

<h1 class="page-header">
	Paginacion de resultados
</h1>
<p>El numero de registros es {!! $users->total() !!}</p>
<p>Pagina  {!! $users->currentPage() !!} de {!! $users->lastPage() !!}</p>

<table class="table table-hover table-striped">
	<thead>
		<tr>
			<th>ID</th>
		<th>Nombre</th>
		<th>Email</th>
		<th>Genero</th>
		<th>Biografia</th>
			
		</tr>
		
	</thead>
	<tbody>

		@include('query.partials.list-users')
		
	</tbody>

</table>

{!! $users->render() !!}
</div>



@stop
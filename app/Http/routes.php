<?php

use Faker\Factory as Faker;

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 
	[
		'as'=>'home',
		'uses'=>'PageController@home'
]);

Route::get('/all', [
		'as'=>'all',
		'uses'=>'QueryController@getAll'
]);

Route::get('/get/{gender}', [
		'as'=>'get',
		'uses'=>'QueryController@getGet'
]);
Route::get('/get', [
		'as'=>'get-custom',
		'uses'=>'QueryController@getGetCustom'
]);

Route::delete('/delete/{id}', [
		'as'=>'delete',
		'uses'=>'QueryController@getDelete'
]);

Route::get('/list', [
		'as'=>'list',
		'uses'=>'QueryController@getList'
]);

Route::get('/first-last', [
		'as'=>'first-last',
		'uses'=>'QueryController@getFirstLast'
]);

Route::get('/paginate', [
		'as'=>'paginate',
		'uses'=>'QueryController@getPaginate'
]);


Route::get('nuevouser', function () {

	$faker = Faker::create();
    $user =  App\User::create([
    	'name'=> $faker->name,
    	'email'=>$faker->email,
    	'password'=>bcrypt('123456'), 
    	'gender'=>$faker->randomElement(['f','m']), 
    	'biography'=>$faker->text(255)
    ]);
    return $user;
});

Route::get('leeruser/{id}', function ($id) {

    $user =  App\User::find($id);

    
    return $user;
});

Route::get('updateuser/{id}', function ($id) {

    $user =  App\User::find($id);

    	$user->name = 'David C. Perdomo';
    	$user->email = 'dperdomo@imagegroup.ws';
    	$user->password = bcrypt('123456'); 
    	$user->gender = 'm'; 
    	$user->biography = 'Aprendiz permanente por siempre';
    	$user->save();

    return "Usuario Actualizado";
});


Route::get('borraruser/{id}', function ($id) {

    $user =  App\User::find($id);
    $user->delete();
    
    return 'Usuario con id '.$id.' fue eliminado';
});
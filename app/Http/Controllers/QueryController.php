<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;

class QueryController extends Controller
{
    //

    public function getAll(){


    	$users = User::all();
$titulo ="Todos los registros (ALL)";

    	return view('query.metodos', compact('titulo', 'users'));
    }

    public function getGet($gender){


    	$users = User::where('gender', $gender )->get();
    	$titulo ="Registros con filtro (GET)";

    	return view('query.metodos', compact('titulo', 'users'));
    } 
    public function getGetCustom(){


    	$users = User::where('gender','f' )->get([
    		'id','name','email'
    	]);
    	$titulo ="Get Personalizado(GET) con Array";

    	return view('query.metodos', compact('titulo', 'users'));
    }

     public function getDelete($id){


    	$user = User::find($id);
    	$user->delete();

    	$titulo ="Get Personalizado(GET) con Array";

    	return view('pages.delete');
    }

    public function getList(){


    	$users = User::orderBy('name','asc')
    					->lists('name','id');

    	return view('query.list', compact('users'));

	}
	public function getFirstlast(){

		$first = User::first();

		$all = User::all();
		$last = $all->last();

    	return view('query.first-last', compact('first','last'));

	}

	 public function getPaginate(){


    	$users = User::orderBy('id','DESC')
    					->paginate();

    	return view('query.paginacion', compact('users'));

	}
}

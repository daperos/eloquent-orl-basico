<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;

class PageController extends Controller
{
    //
    public function home(){
    	$users = User::orderBy('id','desc')
    			->take(10) //limita el numero de resultados
    			->get();

    	return view('pages.home', compact('users'));
    }
}

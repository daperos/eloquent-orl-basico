<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(User::class)->create(
        	[
        		'name'=>'Nombre Personalizado',
        		'email'=>'mensaje@imagegroup.ws'
        	]
        );
        factory(User::class, 100)->create();
    }
}
